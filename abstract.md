Relations are basic mathematical objects
that are omnipresent in computer science.
Describing graph structures,
or speaking of semantics of programs
implies to manipulate them.
We study here binary relations on words,
namely transductions,
that are computed by different kinds of transducers,
beyond the rational transductions.
Our main focus is the class of transductions
that are realized by two-way nondeterministic transducers.

Though determinism (or unambiguity)
yields a robust class of word-to-word functions,
the situation is more complex
in the case of nondeterministic transducers.
Indeed,
the dynamics of such devices
are difficult to capture
since their successful computations
may be arbitrarily long.
We discuss two approaches to describe
nonfunctional transductions realized by two-way transducers.

The first approach is algebraic.
We introduce natural operators
that mimic the abilities of two-way transducers
in a similar way as
rational operations mimic abilities of one-way transducers.
These operations are sufficient to capture
some families of transductions realized by restricted versions of transducers,
such as sweeping transducers,
i.e., transducers in which the input head
is able to change direction
only at the boundaries of the input.
Another so-captured subclass of transductions,
which will be the central part of our presentation,
is defined by unary transducers,
i.e., unrestricted two-way transducers
working over single-letter input and output alphabets.

The second approach is obtained by enriching the semantics of transducers.
We consider transductions with origin information,
that intuitively says
which output position depends on which input position.
We briefly discuss how this enriched semantics
helps in recovering some decidability results,
and we present other classes of nonfunctional transductions
that can be characterized through this information.
